package com.flytxt;

public class Execute {

    public static void main(final String[] args) {
        final String marathonHost = args[0];
        final AppKill ap = new AppKill(marathonHost, "flyuser", "flypassWORD");
        ap.execute();
    }

}
