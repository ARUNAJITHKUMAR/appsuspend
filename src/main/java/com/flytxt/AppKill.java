package com.flytxt;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;

import mesosphere.marathon.client.Marathon;
import mesosphere.marathon.client.MarathonClient;
import mesosphere.marathon.client.model.v2.App;
import mesosphere.marathon.client.model.v2.Task;

public class AppKill {

    String marathonUrl;

    String marathonUserName;

    String marathonPassword;

    List<Task> l = new ArrayList<Task>();

    List<App> appsMarathon = new ArrayList<App>();

    private static Log log;

    Marathon marathon;

    public AppKill(final String marathonUrl, final String marathonUserName, final String marathonPassword) {
        this.marathonPassword = marathonPassword;
        this.marathonUrl = marathonUrl;
        this.marathonUserName = marathonUserName;
    }

    public static Log getLog() {
        if (log == null) {
            log = new SystemStreamLog();
        }

        return log;
    }

    static String trimLeadingSlash(final String appId) {
        if (appId != null && appId.startsWith("/")) {
            return appId.substring(1);
        } else {
            return appId;
        }
    }

    void stopApps(final List<App> appsMarathon) {
        for (final App app : appsMarathon) {
            app.setInstances(0);
            try {
                marathon.updateApp(trimLeadingSlash(app.getId()), app, false);
            } catch (final Exception e) {
                getLog().info("Failed to update " + app.getId());
            }

        }
    }

    void execute() {
        marathon = MarathonClient.getInstanceWithBasicAuth(marathonUrl, marathonUserName, marathonPassword);

        for (final App apps : marathon.getApps().getApps()) {

            if (apps.getId().startsWith("/neon/apps/")) {

                appsMarathon.add(apps);
            }
        }
        final Properties props = new Properties();
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream("properties.yml");
        } catch (final FileNotFoundException e) {
            e.printStackTrace();
        }
        for (final App apps : appsMarathon) {
            props.setProperty(apps.getId(), apps.getInstances().toString());

        }
        try {
            props.store(fos, "Neon Apps Found in MarathonEnvi.");
        } catch (final IOException e) {
            e.printStackTrace();
        }
        stopApps(appsMarathon);

    }

}
